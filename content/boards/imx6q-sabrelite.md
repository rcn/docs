---
title: i.MX6 Quad Sabre Lite
---

The i.MX6 Sabrelite back in the day was the "official" low-cost development
board for the i.MX6 SoC family. These days it's known as the BD-SL-i.MX6.

![I.mx6 Sabre lite with serial cable](/img/imx6q-sabrelite.jpg)

specification:
* NXP i.MX6 Quad SOC
* 4x ARM Cortex-A9 @ 1ghz
* 1GB memory
* Etnaviv GC200 GPU
* Ethernet port
* 2x USB
* 1x full-size SD card, 1x micro SD card
* 1x SATA II port
* No RTC
* Size: 82x82mm

Vendors for the board can be found via its 
[boundary devices page](https://boundarydevices.com/product/bd-sl-i-mx6/). When
buying new ones we likely need to order the "kit" as that comes with a serial
cable, it's unclear if the board-only offering has that as well.

### Power control

The board can be powered on/off via a DC power jack:
* 5V/4A DC input, 2.1mm x 5.5mm, center positive

### Low-level boot control

The "serial cable" included in the kit expands to two DB-9 connectors. One of
which is connected to the serial debug console. Labelling has been inconsistent
so it's a matter of trying whichone works.

Specifications:
* RS232/DB9 serial cable
* Serial port settings: 115200 8n1

#### Network connectivity

There is a standard ethernet jack on the device that can be used. However
support for gigabit speeds has been flaky. For reliably networking the
port has to be forced to 100mbit/s. Generally best results with using a
100mbit/s only switch rather then a gigabit capable switch.

### Bootloader

For the bootloader the Apertis u-boot build should be used (as it allows for
testing apertis specifics). Documentation on how to install this can be found
on the
[apertis setup page](https://www.apertis.org/reference_hardware/imx6q_sabrelite_setup/)

U-boot is stored inthe SPI flash so cannot be corrupted by mass storage usage

U-boot load address to be used for bootm and bootz are:
* Kernel: 0x10000000
* DTB: 0x13f00000
* ramdisk: 0x14000000

### Health checks

* Supported Linux kernel ARM defconfig since many years (at least since 4.0)

### Lab notes and trouble shooting

Insert SD cards in both SD card slots (micro and full size) to be used for
testing.
